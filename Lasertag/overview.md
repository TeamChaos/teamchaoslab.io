<p style="text-align:center;"><img src="img/logo2.svg" width="800px"/></p>

<br>

## Overview

The goal of our project is to create the world's best Lasertag system.  

There are many different commercial Lasertag systems available. However, they are all very limited in functionality, reconfigurability and/or openness. Besides having fun developing and building our own system, we are creating an easily extensible, extensively configurable and feature rich system, all while trying to maintain a reasonable price tag. It is probably not going to be cheaper than most low-cost commercial systems, yet it's openness and configurability should allow to easily create a slimmed-down version.

### Features

The following is an incomplete list of the major available or planned features:

- Accurate, long distance hit detection featuring a 360° headband and a multi-detector vest
- Many different game modes like Free for All, Conquest, Capture the Flag, TDM, Gungame and more
- Different tagger types (shotgun, assault rifle, sniper) as well as grenades, claymores and similar
- Level elements like spawn points, medkits, ammunition packs or capture points
- Sound, light and vibration feedback where possible
- Configuration and control via responsive web interface
- Live stats display on optional personal mobile web interface
- Voice feedback, voice communication, game overview on optional Android application

The system is designed to be used in rather open areas like forests or empty industrial buildings.
Although, the system is not water proof at the moment.

### Alternatives

There are a couple of similar (open source) projects. The most notable is the _Miles Tag_(tm) system (http://www.lasertagparts.com/mtdesign.htm). The self-proclaimed "Leader in D.I.Y Lasertag since 2003" also offers a feature rich Laser Tag system with an established community.

While the Miles Tag system is focused around the weapon itself, we wanted to put the player (controller device) in the center. With a detector vest and headband as well as the option to change weapons mid-game this allows for more possibilities, but makes the system more complex to use of course.

In the more professional (and closed source) sector there is e.g. https://www.icombat.com/. They offer different powerful systems. This includes level elements, wireless communications and an extensive configuration and statistics interface. However, this is mostly focused on franchise based lasertag sites.

Our system should prove as a viable alternative at some point in the future.

Other projects:

- [Skirmos](https://www.kickstarter.com/projects/skirmos/skirmos-open-source-laser-tag/description) (apparently dead, allegedly open source)
- [OpenLaserTag](https://github.com/lucullusTheOnly/OpenLaserTag) (limited functionality and documentation)

### Legal consideration of public usage

We are still investigating the issues of using the system in public (but remote) areas (in Germany).

Our current prototype tagger uses a plastic toy weapon imitation based on a M16 or similar.
Even though the tagger is smaller, colorful, makes SciFi noises and does not shoot ;), it may be mistaken for something real by strollers.

## Overview

You can find an overview about the different aspects using the navigation on the left or the links below

#### Concept

[Concept](https://teamchaos.gitlab.io/Lasertag/concept.html) (and parts)

#### Hardware

[Hardware Overview](https://teamchaos.gitlab.io/Lasertag/hardware.html)

#### Software

[Software Overview](https://teamchaos.gitlab.io/Lasertag/software.html)

## Links

[***Issue board***](https://gitlab.com/groups/TeamChaos/LaserTag/-/boards) For an overview of the specific parts we are currently working on, have a look at the issue board. Issues tagged with 'Discussion' are used for discussing and writing down general conceptual decisions.

[***Source code***](https://gitlab.com/TeamChaos/LaserTag) The source code and the hardware schematics/layouts are hosted on gitlab.com.

[***Repository Overview***](https://gitlab.com/TeamChaos/LaserTag/Concept) This repo serves as a place for general concepts, issues and navigation.

