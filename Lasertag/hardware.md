# Hardware (outdated)
<br>
<img src="img/overview_hardware.svg" width="800px"/>
<br><br>
## Boards
The project currently consists of several different PCBs:

- The [Main PCB](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/MainPCB) (_player controller board_) containing the _player controller_.
- The [VestMountingPlate PCB](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/VestMountingPlate) providing the mounting point for the Main PCB and connecting it to the vest.
- The [HID PCB](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/HidPCB) handling the user interactions (LEDs and buttons) and connected to the Main PCB.
- The [Weapon PCB](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/WeaponPCB) installed in the weapon.
- The [Receiver PCB](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/ReceiverPCB) which only holds the IR receiver module.
- The [Generic PCB](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/GenericPCB) used for generic IR beacons.

All equipment used by the player operates from a generic USB power bank. The USB power is surge-protected on the Main PCB and the resulting 5V VDD voltage is distributed to all boards required.
Additionally, a 3.3V supply voltage is generated using a linear regulator on the Main PCB.
An I2C bus is used for connecting several components on the MainPCB as well as the HID and weapon. It operates at 3.3V, but there is a level shifter on the Main PCB to allow the 5V only weapon to access it.

### Main PCB
The central component of the system is the main PCB as it features the power management, the _player controller_ (ESP32), the IR signal processing IC, a sound chip and a speaker, a power driver for the vibration motors and more.

### HID PCB
The small HID PCB is connected to the Main PCB using a ribbon cabble and features a LED controller chip. The LED controller is connected via I2C, the buttons are connected directly.

### Weapon PCB
The tagger PCB is mostly independent and placed inside the tagger. It is only connected via 4 wires (5V, SDA, SCL, GND) to the Main PCB (via the VestMountingPlate).
An Atmel Attiny microcontroller is the heart of the board and controlls the attached LEDs, buttons and speaker. A large capacitance and transistor drivers are used to provide the necessary output current for the IR and flash LED.

### Generic PCB
This PCB operates completely independently and can be powered from a variable voltage around 3V.  
An Atmel Attiny microcontroller controls the main operation and drives e.g. the attached IR LEDs. A 6 bit switch can be used to indicate the desired operation to the microcontroller.

## Connectors

#### General

There are different connections used to connect all components. All connections use connectors of the JST XH series on the Main PCB side.

##### Headband Connection

The 4 IR receivers in the headband and the corresponding vibration motors are connected using a 10 core wire. The configuration is as follows:

<p style="text-align:center;"><img src="img/pinout_ir_receiver_head_plug.svg" width="300px"/></p>

##### Single Detector Connection

The remaining receivers are connected individually using a 3 core wire and soldered onto the VestMountingPCB together with the 2 core wires of the 2 vest vibration motors..

##### Weapon Connection

The weapon is connected using a 4 core wire which is soldered onto the VestMountingPCB.

There is a USB A connector inline allowing the weapon to be easily disconnected.

The power lines are expected to face larger peak currents up to maybe 1A for a very short time period.

##### Supply Connection

The power supply is connected using a simple 2 core wire soldered onto the VestMountingPCB. There is a USB A connector so it can be plugged into a power bank.

#### Debugging

There are two different debugging/programming headers.

##### Attiny IC

To debug/program the Attiny ICs there is a `AVRprog` header on the main as well as the weapon PCB. The basic configuration is as follows:

<p style="text-align:center;"><img src="img/pinout_avr_programmer.svg" width="100px"/></p>
**However**, the VDD pin is not connected on the Main PCB so it has to be powered using the standard supply. The weapon PCB should be disconnected from the Main PCB during programming.

Since there are two different Attiny ICs on the Main PCB so there is a jumper, which connects the RESET line with only one of the two ICs.

##### ESP32

There is one ESPprogrammer header available to programm the ESP itself. The configuration is as follows:

<p style="text-align:center;"><img src="img/pinout_esp_programmer.svg" width="110px"/></p>
The first two I2C pins are only available for debugging. Programming is done with the remaining 4 pins. If 5V are supplied here, the Main PCB should be disconnected from the main power supply.

### Pin Configuration

The pinout of the individual ICs can be found in the corresponding README file

## Images (Outdated)

##### MainPCB



<img src="img/MainPCB2_commented.png" width="800px"/><img src="img/MainPCB2-withoutESP.png" width="800px"/>

##### Weapon PCB

<img src="img/WeaponPCB-Commented.png" width="600px"/>

##### Receiver PCB

<img src="img/detectorPCB.png" width="600px"/>

Used to (Inter)connect detectors, vibration motors and LEDs in headband.

Can also be used to attach and connect detectors in vest. 



##### Vest and headband

##### <img src="img/vest.png" width="700px"/>

------

<p style="text-align:center;"><img src="img/logo2.svg" width="800px"></p>
