# Concept
Each player has a _player controller board_ which is the central unit for the player.
A kit additionally consists of a tagger, a receiver headband, a receiver harness/vest and a power bank. These components are connected to the _player controller board_ via an appropriate plug.  

The "shooting" is implemented using standard IR transmission techniques (950nm, 38kHz carrier, on-off keying at lower frequency).

The signal is only detected using the vest and headband. The weapon itself does not include a receiver.

A secondary (radio) transmission system (433MHz, on-off keying at lower frequency) is used as an omni-directional communication system for game updates and inter-player communication.
Alternatively, an internet based system can be used. For this the player requires a smartphone with a data connection which provides a WiFi access point for their _player controller board_.

A central base station is used to configure, prepare, control and evaluate the game. It can be either a local raspberry communicating either via a local hotspot and 433 MHz RF or, if using the internet based system, a server.

### Tagger
For the weapon we use a (rather) cheap plastic toy weapon which has a built-in speaker, a trigger and mounting points for LEDs.  However, we replace all built-in electronics with our own and cut away some plastic parts.

A flexible spiral cable (USB cable) is used to connect the tagger to the PMD. It is led along the players shoulder and connected to an USB plug connected to the _player controller board_. The USB standard is not used though. This allows the player to easily connected and disconnect the tagger.

Prototype:

<p style="text-align:center;"><img src="img/weapon-prototype.png" width="800px"></p>
<p style="text-align:center;"><img src="img/weapon-internal.png" width="800px"></p>

### Harness
The harness features a mounting point for the _player controller board_. It is a PCB with the receiver, the power and weapon connections soldered onto. Three pin headers provide the electrical and mechanical connection to the _player controller board_.  
It also includes 6 IR receiver modules (4 on the front, 2 on the back) and 2 vibration motors (1 on the front, 1 on the back).

### Headband
The headband holds 4 IR receivers and 4 vibration motors, one for each side, and is sewed using flexible strap.
It is connected to the _player controller board_.


Schematic view:

<p style="text-align:center;"><img src="img/gurt.png" width="800px"></p>



### System network

<img src="img/software-network.svg" width="800px"/>

------

<p style="text-align:center;"><img src="img/logo2.svg" width="800px"></p>
