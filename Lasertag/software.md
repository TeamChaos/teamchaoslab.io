# Software and Protocols

<br>
<img src="img/software-network.svg" width="800px"/>
<br><br>

This project is powered by quite a few microcontrollers and applications which communicate via different protocols.

### Devices and Software

There are several different devices that run their own program.

- [PlayerController](https://gitlab.com/TeamChaos/LaserTag/PlayerController/tree/master/) ESP32 which controls all player specific functionality
- [BaseServer](https://gitlab.com/TeamChaos/LaserTag/baseserver) NodeJS based Basestation running either on raspberry or on an online server which controls the game setup and execution
- Mobile application which display live game information
- The [Weapon](https://gitlab.com/TeamChaos/LaserTag/Weapon) Attiny IC which controls the weapons functionality
- The [Detector](https://gitlab.com/TeamChaos/LaserTag/Detector) Attiny IC which handles the IR signal detection and processing
- The [GenericSender](https://gitlab.com/TeamChaos/LaserTag/genericsender) Attiny IC on the GenericSender PCB providing a configurable beacon functionality


### Connections and Protocols

These programs communicate with each other via individual connections and dedicated connections:

- [Weapon Control](https://gitlab.com/TeamChaos/LaserTag/Concept/blob/master/Network/Protocols/WeaponControl.md) [(C header)](https://gitlab.com/TeamChaos/LaserTag/CommonLib/blob/master/weapon_control/protocol.h): I2C between PMD and Weapon IC
  - Master/Slave - Communication only initiated by PMD
- [Detector Control](https://gitlab.com/TeamChaos/LaserTag/Concept/blob/master/Network/Protocols/DetectorControl.md) [(C header)](https://gitlab.com/TeamChaos/LaserTag/CommonLib/blob/master/detector_control/protocol.h): I2C between PMD and Detector IC
  - Master/Slave - Communication only initiated by PMD
  - Additional GPIO) line to allow the detector to indicate available data
- [Game Control/Game WS](https://gitlab.com/TeamChaos/LaserTag/baseserver/blob/master/src/server/network/impl/messages.ts): TCP websocket via cellular network or 433MHz between Basestation and PMD during game phase
  - "Connectionless" - Single messages broadcast (websocket messages are broadcast to other websockets serverside)
  - Low bandwith, collisions possible (on 433MHz)
- [Setup Control/ESP API](https://gitlab.com/TeamChaos/LaserTag/baseserver/blob/master/src/server/routes/esp.ts): HTTP(s) based communication over WiFi or cellular between Basestation and player controller during setup phase
- [App WS](https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/app_ws_message.schema.json): TCP websocket over WiFi or cellular between web/mobile application and baseserver during game phase
- [IR Transmission](https://gitlab.com/TeamChaos/LaserTag/Concept/blob/master/Network/Protocols/IRProtocol.md) [(C header)](https://gitlab.com/TeamChaos/LaserTag/CommonLib/blob/master/ir/protocol.h): Modulated IR signal "shooting" from weapon to detector
  - 940nm - toggled with 38kHz and modulated with low frequency on/off keying
  - Low bandwith, collisions possible



------

<p style="text-align:center;"><img src="img/logo2.svg" width="800px"></p>

